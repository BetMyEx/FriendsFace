//
//  FriendsFaceApp.swift
//  FriendsFace
//
//  Created by Admin on 09.03.2022.
//

import SwiftUI

@main
struct FriendsFaceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
