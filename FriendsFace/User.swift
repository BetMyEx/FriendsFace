//
//  User.swift
//  FriendsFace
//
//  Created by Admin on 09.03.2022.
//

import Foundation

struct User: Codable, Identifiable {
    var id: String
    var isActive: Bool
    var name: String?
    var age: Int
    var company: String?
    var email: String?
    var address: String?
    var about: String?
    var registered: String?
    var tags: [String]?
    var friends: [Friend]?
    
    var unwrappedName: String {
        name ?? "Unknown"
    }
    var unwrappedCompany: String {
        company ?? "Unknown"
    }
    var unwrappedEmail: String {
        email ?? "Unknown"
    }
    var unwrappedAddress: String {
        address ?? "Unknown"
    }
    var unwrappedAbout: String {
        about ?? "Unknown"
    }
    var unwrappedRegistered: String {
        registered ?? "Unknown"
    }
    var unwrappedTags: [String] {
        tags ?? ["Unknown"]
    }
    var unwrappedFriends: [Friend] {
        friends ?? []
    }
    
    
    
}

struct Friend: Codable, Identifiable {
    var id: String
    var name: String?
}
