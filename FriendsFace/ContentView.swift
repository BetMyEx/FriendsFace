//
//  ContentView.swift
//  FriendsFace
//
//  Created by Admin on 09.03.2022.
//

import SwiftUI

struct ContentView: View {
    @State private var users = [User]()
    var body: some View {
        NavigationView{
            List(users){ user in
                VStack(alignment: .leading) {
                        Text(user.unwrappedName)
                            .font(.title2)
                    
                }
            }
        }.task {
            await fetchData()
        }
    }
    func fetchData() async {
        guard let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json") else {
            print("Invalid URL")
            return
        }
        print(url)
        do {
            let (data, _) = try await URLSession.shared.data(from: url)

            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            if let decodedResponse = try? decoder.decode([User].self, from: data) {
                users = decodedResponse
            } else {
                print("Decode error")
            }
        } catch {
            print("Invalid data")
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
